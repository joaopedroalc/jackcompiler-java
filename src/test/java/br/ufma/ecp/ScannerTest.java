package br.ufma.ecp;

import br.ufma.ecp.scanner.Scanner;
import br.ufma.ecp.scanner.token.Token;
import br.ufma.ecp.scanner.token.TokenType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class ScannerTest extends TestSupport {
    @Test
    void testScannerWithSquareGame() throws IOException {
        var input = fromFile("lexer/SquareGame.jack");
        var expectedResult = fromFile("lexer/SquareGameT.xml");

        var scanner = new Scanner(input.getBytes(StandardCharsets.UTF_8));
        var result = new StringBuilder();
        var tokens = new ArrayList<Token>();

        while (scanner.hasNextToken()) {
            tokens.add(scanner.nextToken());
        }

        result.append("<tokens>\r\n");
        tokens.stream()
                .filter(token -> token.getType() != TokenType.EOF)
                .forEach(token -> result.append(token).append("\r\n"));
        result.append("</tokens>\r\n");

        Assertions.assertEquals(expectedResult, result.toString());
    }

    @Test
    void testScannerWithSquare() throws IOException {
        var input = fromFile("lexer/Square.jack");
        var expectedResult = fromFile("lexer/SquareT.xml");

        var scanner = new Scanner(input.getBytes(StandardCharsets.UTF_8));
        var result = new StringBuilder();
        var tokens = new ArrayList<Token>();

        while (scanner.hasNextToken()) {
            tokens.add(scanner.nextToken());
        }

        result.append("<tokens>\r\n");
        tokens.stream()
                .filter(token -> token.getType() != TokenType.EOF)
                .forEach(token -> result.append(token).append("\r\n"));
        result.append("</tokens>\r\n");

        Assertions.assertEquals(expectedResult, result.toString());
    }

    @Test
    void testString(){
        var input = "a = \"Test\"";
        var expectedResult =
                "<tokens>\r\n" +
                "<identifier> a </identifier>\r\n" +
                "<symbol> = </symbol>\r\n" +
                "<stringConst> Test </stringConst>\r\n" +
                "</tokens>\r\n";

        var scanner = new Scanner(input.getBytes(StandardCharsets.UTF_8));
        var tokens = new ArrayList<Token>();
        while (scanner.hasNextToken()) {
            tokens.add(scanner.nextToken());
        }
        var result = new StringBuilder();

        result.append("<tokens>\r\n");
        tokens.stream()
                .filter(token -> token.getType() != TokenType.EOF)
                .forEach(token -> result.append(token).append("\r\n"));
        result.append("</tokens>\r\n");
        Assertions.assertEquals(expectedResult, result.toString());
    }
}
