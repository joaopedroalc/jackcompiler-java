package br.ufma.ecp.parser;

import br.ufma.ecp.parser.node.Node;
import br.ufma.ecp.parser.node.NodeParent;
import br.ufma.ecp.scanner.Scanner;
import br.ufma.ecp.scanner.token.*;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class Parser {

    private Scanner scan;
    private Token currentToken;
    private Token peekToken;

    public Parser(byte[] input) {
        scan = new Scanner(input);
        peekToken = scan.nextToken();
        nextToken();
    }

    public Token getCurrentToken() {
        return currentToken;
    }

    public Token getPeekToken() {
        return peekToken;
    }

    private void nextToken() {
        currentToken = peekToken;
        peekToken = scan.nextToken();
    }

    public Node parse() {
        if (currentToken instanceof KeywordToken) {
            var token = (KeywordToken) this.currentToken;
            return token.getSubType().process(this);
        }
        throw new Error("Keyword is expected, receive: " + currentToken.getLexeme());
    }

    public Token expectPeek(Enum type) {

        if (currentToken.getSubType() == type || currentToken.getType() == type) {
            var token = currentToken;
            nextToken();
            return token;
        }

        throw new Error("Syntax error - expected " + type + " found " + currentToken.getLexeme());

    }

    public static ParseProcessor expression() {
        return parser -> {

            Consumer<Node.Eater> eatNextTerm = eater -> {
                eater.eat(TokenType.SYMBOL);
                eater.eat(term());
            };

            var nodes = Node.eater(parser)
                    .eat(term())
                    .whileMatch(SymbolType::isOperator, eatNextTerm);

            return NodeParent.expression(nodes.spit());
        };
    }

    public static ParseProcessor expressionList() {
        return parser -> {

            if (parser.getCurrentToken().getSubType().equals(SymbolType.RPAREN)) {
                return NodeParent.expressionList(Collections.emptyList());
            }

            Consumer<Node.Eater> eatExpression = eater -> {
                eater.eat(SymbolType.COMMA).eat(expression());
            };

            var nodes = Node.eater(parser)
                    .eat(expression())
                    .whileMatch(token -> token.equals(SymbolType.COMMA), eatExpression);

            return NodeParent.expressionList(nodes.spit());
        };
    }

    public static ParseProcessor term() {
        return parser -> {

            Node.Eater eater = Node.eater(parser);
            eater.eatBySubtypes(List.of(SymbolType.MINUS, SymbolType.NOT));

            if (!eater.spit().isEmpty()) {
                eater.eat(term());
            }

            var current = parser.getCurrentToken();
            var peek = parser.getPeekToken();

            var isSubRoutine =
                    current.getType().equals(TokenType.IDENTIFIER) &&
                            (peek.getSubType().equals(SymbolType.LPAREN) || peek.getSubType().equals(SymbolType.DOT));

            if (isSubRoutine) {
                subRoutineCall(eater, parser);
                return NodeParent.term(eater.spit());
            }

            var isArray = current.getType().equals(TokenType.IDENTIFIER) &&
                    peek.getSubType().equals(SymbolType.LBRACKET);
            if (isArray) {
                eater.eat(TokenType.IDENTIFIER)
                        .eat(SymbolType.LBRACKET)
                        .eat(expression())
                        .eat(SymbolType.RBRACKET);
                return NodeParent.term(eater.spit());
            }

            var isExpression = current.getSubType().equals(SymbolType.LPAREN);
            if (isExpression) {
                eater.eat(SymbolType.LPAREN)
                        .eat(expression())
                        .eat(SymbolType.RPAREN);
                return NodeParent.term(eater.spit());
            }

            var nodes = eater.eatByTypes(
                    List.of(TokenType.IDENTIFIER, TokenType.STRING, TokenType.NUMBER, TokenType.KEYWORD)
            );

            return NodeParent.term(nodes.hasAtLeast(1).spit());
        };
    }

    public static ParseProcessor let() {
        return parser -> {
            var nodes = Node.eater(parser)
                    .eat(KeywordType.LET)
                    .eat(TokenType.IDENTIFIER)
                    .eat(SymbolType.LBRACKET, expression(), SymbolType.RBRACKET)
                    .eat(SymbolType.EQ)
                    .eat(expression())
                    .eat(SymbolType.SEMICOLON)
                    .spit();

            return NodeParent.let(nodes);
        };
    }

    public static ParseProcessor whileStatement() {
        return parser -> {

            var nodes = Node.eater(parser)
                    .eat(KeywordType.WHILE)
                    .eat(SymbolType.LPAREN)
                    .eat(expression())
                    .eat(SymbolType.RPAREN)
                    .eat(SymbolType.LBRACE)
                    .eat(statements())
                    .eat(SymbolType.RBRACE);

            return NodeParent.whileStatement(nodes.spit());
        };
    }

    public static void subRoutineCall(Node.Eater nodes, Parser parser) {

        nodes.eat(TokenType.IDENTIFIER);

        if (parser.getCurrentToken().getSubType().equals(SymbolType.DOT)) {
            nodes.eat(SymbolType.DOT)
                    .eat(TokenType.IDENTIFIER);
        }

        nodes.eat(SymbolType.LPAREN)
                .eat(expressionList())
                .eat(SymbolType.RPAREN);
    }

    public static ParseProcessor doStatement() {
        return parser -> {

            var nodes = Node.eater(parser)
                    .eat(KeywordType.DO);

            subRoutineCall(nodes, parser);

            nodes.eat(SymbolType.SEMICOLON);

            return NodeParent.doStatement(nodes.spit());
        };
    }

    public static ParseProcessor returnStatement() {
        return parser -> {

            var nodes = Node.eater(parser)
                    .eat(KeywordType.RETURN);

            if (parser.getCurrentToken().getSubType().equals(SymbolType.SEMICOLON)) {
                nodes.eat(SymbolType.SEMICOLON);
                return NodeParent.returnStatement(nodes.spit());
            }

            nodes.eat(expression())
                    .eat(SymbolType.SEMICOLON);

            return NodeParent.returnStatement(nodes.spit());
        };
    }

    public static ParseProcessor ifStatement() {
        return parser -> {

            var nodes = Node.eater(parser)
                    .eat(KeywordType.IF)
                    .eat(SymbolType.LPAREN)
                    .eat(expression())
                    .whileMatch(SymbolType::isOperator, eater -> eater.eat(expression()))
                    .eat(SymbolType.RPAREN)
                    .eat(SymbolType.LBRACE)
                    .eat(statements())
                    .eat(SymbolType.RBRACE);

            if (parser.getCurrentToken().getSubType() == KeywordType.ELSE) {
                nodes.eat(KeywordType.ELSE)
                        .eat(SymbolType.LBRACE)
                        .eat(statements())
                        .eat(SymbolType.RBRACE);
            }

            return NodeParent.ifStatement(nodes.spit());
        };
    }

    public static ParseProcessor statements() {
        return parser -> {
            Consumer<Node.Eater> compileStatements = eater -> {
                var currentToken = parser.getCurrentToken();
                if (!(currentToken instanceof KeywordToken)) {
                    throw new Error("Keyword is expected, receive: " + currentToken.getLexeme());
                }
                var token = (KeywordToken) currentToken;
                eater.eat(token.getSubType().process(parser));
            };

            var nodes = Node.eater(parser)
                    .whileMatch(token -> !token.equals(SymbolType.RBRACE), compileStatements);

            return NodeParent.statements(nodes.spit());
        };
    }

    public static ParseProcessor classDec() {
        return parser -> {

            var nodes = Node.eater(parser)
                    .eat(TokenType.KEYWORD)
                    .eat(TokenType.IDENTIFIER)
                    .eat(SymbolType.LBRACE)
                    .whileMatch(token -> !token.equals(KeywordType.CONSTRUCTOR), eater -> eater.eat(classVarDec()))
                    .whileMatch(token -> !token.equals(SymbolType.RBRACE), eater -> eater.eat(subRoutineDec()))
                    .eat(SymbolType.RBRACE);

            return NodeParent.classDef(nodes.spit());
        };
    }

    public static ParseProcessor parameterList() {
        return parser -> {

            if (parser.getCurrentToken().getSubType().equals(SymbolType.RPAREN)) {
                return NodeParent.parameterList(Collections.emptyList());
            }

            List<Enum> types = List.of(TokenType.IDENTIFIER, TokenType.KEYWORD);

            var nodes = Node.eater(parser)
                    .eatByTypes(types);
            nodes
                    .eat(TokenType.IDENTIFIER)
                    .whileMatch(
                            token -> token.equals(SymbolType.COMMA),
                            eater -> {
                                eater.eat(SymbolType.COMMA)
                                        .eatByTypes(types)
                                        .eat(TokenType.IDENTIFIER);
                            }
                    );

            return NodeParent.parameterList(nodes.spit());
        };
    }


    public static ParseProcessor subRoutineDec() {
        return parser -> {

            var nodes = Node.eater(parser)
                    .eatBySubtypes(List.of(KeywordType.CONSTRUCTOR, KeywordType.FUNCTION, KeywordType.METHOD))
                    .hasAtLeast(1)
                    .eatBySubtypes(List.of(KeywordType.VOID, TokenType.IDENTIFIER))
                    .hasAtLeast(2)
                    .eat(TokenType.IDENTIFIER)
                    .eat(SymbolType.LPAREN)
                    .eat(parameterList())
                    .eat(SymbolType.RPAREN)
                    .eat(subRoutineBody());


            return NodeParent.subRoutineDec(nodes.spit());
        };
    }

    public static ParseProcessor subRoutineBody() {
        return parser -> {
            var nodes = Node.eater(parser)
                    .eat(SymbolType.LBRACE);

            var hasVarDeclaration = parser.getCurrentToken().getSubType().equals(KeywordType.VAR);
            if (hasVarDeclaration) {
                nodes.eat(varDec())
                        .whileMatch(token -> token.equals(KeywordType.VAR), eater -> eater.eat(varDec()));
            }

            nodes.eat(statements())
                    .eat(SymbolType.RBRACE);
            return NodeParent.subRoutineBody(nodes.spit());
        };
    }

    public static ParseProcessor varDec() {
        return parser -> {

            var nodes = Node.eater(parser)
                    .eat(KeywordType.VAR)
                    .eatByTypes(List.of(TokenType.KEYWORD, TokenType.IDENTIFIER))
                    .eat(TokenType.IDENTIFIER)
                    .eat(SymbolType.SEMICOLON);

            return NodeParent.varDec(nodes.spit());
        };
    }

    public static ParseProcessor classVarDec() {
        return parser -> {

            var nodes = Node.eater(parser)
                    .eat(KeywordType.FIELD)
                    .eatByTypes(List.of(TokenType.KEYWORD, TokenType.IDENTIFIER))
                    .eat(TokenType.IDENTIFIER)
                    .whileMatch(token -> token.equals(SymbolType.COMMA), eater -> {
                        eater.eat(SymbolType.COMMA).eat(TokenType.IDENTIFIER);
                    })
                    .eat(SymbolType.SEMICOLON);

            return NodeParent.classVarDec(nodes.spit());
        };
    }

    public static ParseProcessor defaultParser() {
        return parser -> {
            throw new Error("Not implemented");
        };
    }

}
