package br.ufma.ecp.parser.node;

import java.util.ArrayList;
import java.util.List;

public class NodeParent extends Node {
    private List<Node> nodes = new ArrayList<>();
    private String description;

    public NodeParent(List<Node> nodes, String description) {
        this.nodes.addAll(nodes);
        this.description = description;
    }

    public String nodesToString() {
        var str = new StringBuilder();
        nodes.forEach(
                node ->
                        str.append(
                                node.toString()
                                        .replaceAll("(^)", "  $1")
                                        .replaceAll("\r\n", "\r\n  ")
                        ).append("\r\n")
        );
        return str.toString();
    }

    @Override
    public String toString() {
        return
                String.format("<%s>\r\n", description) +
                nodesToString() +
                String.format("</%s>", description);

    }

    public static NodeParent let(List<Node> nodes) {
        return new NodeParent(nodes, "letStatement");
    }

    public static NodeParent term(List<Node> nodes) {
        return new NodeParent(nodes, "term");
    }

    public static NodeParent ifStatement(List<Node> nodes) {
        return new NodeParent(nodes, "ifStatement");
    }

    public static NodeParent whileStatement(List<Node> nodes) {
        return new NodeParent(nodes, "whileStatement");
    }

    public static NodeParent statements(List<Node> nodes) {
        return new NodeParent(nodes, "statements");
    }

    public static NodeParent expression(List<Node> nodes) {
        return new NodeParent(nodes, "expression");
    }

    public static NodeParent expressionList(List<Node> nodes) {
        return new NodeParent(nodes, "expressionList");
    }

    public static NodeParent doStatement(List<Node> nodes) {
        return new NodeParent(nodes, "doStatement");
    }

    public static NodeParent returnStatement(List<Node> nodes) {
        return new NodeParent(nodes, "returnStatement");
    }

    public static NodeParent classDef(List<Node> nodes) {
        return new NodeParent(nodes, "class");
    }

    public static NodeParent classVarDec(List<Node> nodes) {
        return new NodeParent(nodes, "classVarDec");
    }

    public static NodeParent subRoutineDec(List<Node> nodes) {
        return new NodeParent(nodes, "subroutineDec");
    }

    public static NodeParent subRoutineBody(List<Node> nodes) {
        return new NodeParent(nodes, "subroutineBody");
    }

    public static NodeParent parameterList(List<Node> nodes) {
        return new NodeParent(nodes, "parameterList");
    }

    public static NodeParent varDec(List<Node> nodes) {
        return new NodeParent(nodes, "varDec");
    }

}
