package br.ufma.ecp.parser.node;

import br.ufma.ecp.scanner.token.Token;

public class TokenNode extends Node {
    private Token token;

    public TokenNode(Token token) {
        this.token = token;
    }

    public static TokenNode build(Token token) {
        return new TokenNode(token);
    }

    @Override
    public String toString() {
        return token.toString();
    }
}
