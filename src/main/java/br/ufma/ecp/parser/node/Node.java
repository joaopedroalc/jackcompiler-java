package br.ufma.ecp.parser.node;

import br.ufma.ecp.parser.ParseProcessor;
import br.ufma.ecp.parser.Parser;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class Node {

    public static Eater eater(Parser parser) {
        return new Eater(parser);
    }

    public static class Eater {
        private Parser parser;
        private List<Node> nodes = new ArrayList<>();

        public Eater(Parser parser) {
            this.parser = parser;
        }

        public Eater eat(Enum type) {
            var token = parser.expectPeek(type);
            nodes.add(TokenNode.build(token));
            return this;
        }

        public Eater eatBySubtypes(List<Enum> types) {
            for (var tokenType : types) {
                if (parser.getCurrentToken().getSubType() == tokenType) {
                    var token  = parser.expectPeek(tokenType);
                    nodes.add(TokenNode.build(token));
                    return this;
                }
            }
            return this;
        }

        public Eater eatByTypes(List<Enum> types) {
            for (var tokenType : types) {
                if (parser.getCurrentToken().getType() == tokenType) {
                    var token  = parser.expectPeek(tokenType);
                    nodes.add(TokenNode.build(token));
                    return this;
                }
            }
            return this;
        }

        public Eater eat(ParseProcessor parseProcessor) {
            nodes.add(parseProcessor.process(parser));
            return this;
        }

        public Eater eat(Enum tokenStart, ParseProcessor parseProcessor, Enum tokenEnd) {
            if (parser.getCurrentToken().getSubType() == tokenStart) {
                eat(tokenStart);
                eat(parseProcessor);
                eat(tokenEnd);
                return this;
            }
            return this;
        }

        public Eater whileMatch(Function<Enum, Boolean> test, Consumer<Eater> func) {
            while (test.apply(parser.getCurrentToken().getSubType())) {
                func.accept(this);
            }
            return this;
        }

        public Eater whileMatch(Enum currentTokenType, Consumer<Eater> func) {
            while (parser.getCurrentToken().getSubType() == currentTokenType) {
                func.accept(this);
            }
            return this;
        }


        public Eater eat(Node node) {
            nodes.add(node);
            return this;
        }

        public Eater hasAtLeast(Integer i) {
            if (nodes.size() < i) {
                throw new Error("Error on parse element. Should have at least " + i + " element");
            }
            return this;
        }

        public List<Node> spit() {
            return nodes;
        }
    }
}
