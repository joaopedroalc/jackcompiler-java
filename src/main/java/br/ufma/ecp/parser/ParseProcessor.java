package br.ufma.ecp.parser;

import br.ufma.ecp.parser.node.Node;

public interface ParseProcessor {
    Node process(Parser parser);
}
