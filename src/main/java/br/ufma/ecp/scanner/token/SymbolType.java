package br.ufma.ecp.scanner.token;

import java.util.Arrays;

public enum SymbolType {
    EQ("="),
    PLUS("+"),
    MINUS("-"),
    ASTERISK("*"),
    SLASH("/"),
    AND("&"),
    OR("|"),
    NOT("~"),
    DOT("."),
    LT("<"),
    GT(">"),
    COMMA(","),
    SEMICOLON(";"),
    LPAREN("("),
    RPAREN(")"),
    LBRACE("{"),
    RBRACE("}"),
    LBRACKET("["),
    RBRACKET("]");

    private String symbol;

    SymbolType(String s) {
        this.symbol = s;
    }

    public String getSymbol() {
        return symbol;
    }

    public static SymbolType fromSymbol(String symbol) {
        return Arrays.stream(SymbolType.values())
                .filter(symbolType -> symbolType.getSymbol().equals(symbol))
                .findFirst()
                .orElse(null);
    }

    public static Boolean isOperator(Enum symbol) {
        if (symbol instanceof SymbolType) {
            return "+-*/<>=&|".contains(((SymbolType) symbol).getSymbol());
        }
        return false;
    }

    public static Boolean isSymbol(char c) {
        return "{}()[].,;+-*/&|<>=~".contains(String.valueOf(c));
    }
}
