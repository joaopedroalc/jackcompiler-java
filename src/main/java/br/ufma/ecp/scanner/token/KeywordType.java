package br.ufma.ecp.scanner.token;

import br.ufma.ecp.parser.ParseProcessor;
import br.ufma.ecp.parser.Parser;
import br.ufma.ecp.parser.node.Node;

import java.util.Arrays;

public enum KeywordType {
    CLASS(Parser.classDec()),
    CONSTRUCTOR(Parser.defaultParser()),
    FUNCTION(Parser.defaultParser()),
    METHOD(Parser.defaultParser()),
    FIELD(Parser.defaultParser()),
    STATIC(Parser.defaultParser()),
    VAR(Parser.varDec()),
    INT(Parser.defaultParser()),
    CHAR(Parser.defaultParser()),
    BOOLEAN(Parser.defaultParser()),
    VOID(Parser.defaultParser()),
    TRUE(Parser.defaultParser()),
    FALSE(Parser.defaultParser()),
    NULL(Parser.defaultParser()),
    THIS(Parser.defaultParser()),
    LET(Parser.let()),
    DO(Parser.doStatement()),
    IF(Parser.ifStatement()),
    ELSE(Parser.defaultParser()),
    WHILE(Parser.whileStatement()),
    RETURN(Parser.returnStatement());

    private ParseProcessor parseProcessor;

    KeywordType(ParseProcessor parseProcessor) {
        this.parseProcessor = parseProcessor;
    }

    public Node process(Parser parser) {
        return parseProcessor.process(parser);
    }

    public static KeywordType from(String lexeme) {
        return Arrays.stream(KeywordType.values())
                .filter(keywordType -> keywordType.toString().equals(lexeme))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
