package br.ufma.ecp.scanner.token;

public class KeywordToken extends Token {

    private KeywordType keywordType;

    public KeywordToken(TokenType tokenType, String lexeme, KeywordType keywordType) {
        super(tokenType, lexeme);
        this.keywordType = keywordType;
    }

    @Override
    public KeywordType getSubType() {
        return keywordType;
    }
}
