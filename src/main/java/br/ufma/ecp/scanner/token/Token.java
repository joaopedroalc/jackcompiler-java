package br.ufma.ecp.scanner.token;

import java.util.Map;

public class Token {
    final TokenType type;
    final String lexeme;

    private Map<String, String> xml = Map.of(
            "<", "&lt;",
            ">", "&gt;",
            "\\", "quot;",
            "&", "&amp;"
    );

    public Token(TokenType tokenType, String lexeme) {
        this.type = tokenType;
        this.lexeme = lexeme;
    }

    public TokenType getType() {
        return type;
    }

    public Enum getSubType() {
        return type;
    }

    public String getLexeme() {
        return lexeme;
    }

    @Override
    public String toString() {
        String categoria = type.getDescription();
        return "<" + categoria + "> " + xml.getOrDefault(lexeme, lexeme) + " </" + categoria + ">";
    }
}
