package br.ufma.ecp.scanner.token;

public class SymbolToken extends Token {
    private SymbolType symbolType;

    public SymbolToken(TokenType tokenType, String lexeme, SymbolType symbolType) {
        super(tokenType, lexeme);
        this.symbolType = symbolType;
    }

    @Override
    public SymbolType getSubType() {
        return symbolType;
    }
}
