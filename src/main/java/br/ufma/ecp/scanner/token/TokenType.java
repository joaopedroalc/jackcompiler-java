package br.ufma.ecp.scanner.token;

public enum TokenType {
    SYMBOL("symbol"),

    NUMBER("integerConstant"),
    IDENTIFIER("identifier"),

    EOF(""),
    ILLEGAL(""),

    // keywords
    KEYWORD("keyword"),
    STRING("stringConst");

    private String description;


    TokenType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
