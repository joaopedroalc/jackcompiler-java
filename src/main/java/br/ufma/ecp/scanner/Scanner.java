package br.ufma.ecp.scanner;

import br.ufma.ecp.scanner.token.*;

import java.nio.charset.StandardCharsets;

import static br.ufma.ecp.scanner.token.TokenType.*;

public class Scanner {
    private byte[] input;
    private int current;
    private int start;

    public Scanner(byte[] input) {
        this.input = input;
        current = 0;
        start = 0;
    }

    private void skipWhitespace() {
        char ch = peek();
        while (ch == ' ' || ch == '\r' || ch == '\t' || ch == '\n') {
            advance();
            ch = peek();
        }
    }

    public Token nextToken() {
        skipWhitespace();

        start = current;
        char ch = peek();

        switch (ch) {
            case '"':
                return readString();

            case '/':
                if (peekNext() == '/') {
                    skipLineComments();
                    return nextToken();
                }
                if (peekNext() == '*') {
                    skipBlockComments();
                    return nextToken();
                }
                var lexeme = Character.toString(peek());
                return new SymbolToken(SYMBOL, lexeme, SymbolType.fromSymbol(lexeme));
            case 0:
                return new Token(EOF, "EOF");
            default:
                if (SymbolType.isSymbol(ch))
                    return symbol();

                if (Character.isDigit(ch)) {
                    return number();
                }

                if (Character.isLetter(ch)) {
                    return identifier();
                }
                advance();
                return new Token(ILLEGAL, Character.toString(ch));
        }

    }

    private void skipLineComments() {
        while (peek() != '\n') {
            advance();
        }
        advance();
    }

    private SymbolToken symbol() {
        var peek = Character.toString(peek());
        advance();
        return new SymbolToken(SYMBOL, peek, SymbolType.fromSymbol(peek));
    }

    private boolean isAlphaNumeric(char ch) {
        return Character.isLetter(ch) || Character.isDigit(ch);
    }

    private Token identifier() {
        while (isAlphaNumeric(peek())) {
            advance();
        }
        var lexeme = new String(input, start, current - start, StandardCharsets.UTF_8);
        try {
            var keywordType = KeywordType.from(lexeme.toUpperCase());
            return new KeywordToken(KEYWORD, lexeme, keywordType);

        } catch (IllegalArgumentException e) {
            return new Token(IDENTIFIER, lexeme);

        }
    }

    private Token number() {
        while (Character.isDigit(peek())) {
            advance();
        }
        String s = new String(input, start, current - start, StandardCharsets.UTF_8);
        return new Token(NUMBER, s);
    }

    public Boolean hasNextToken() {
        return current < input.length;
    }

    private Token readString() {
        advance();
        start = current;
        while (peek() != '"') {
            advance();
        }
        String s = new String(input, start, current - start, StandardCharsets.UTF_8);
        advance();
        return new Token(STRING, s);
    }

    private void advance() {
        char ch = peek();
        if (ch != 0) {
            current++;
        }
    }

    private char peek() {
        if (current < input.length) {
            return (char) input[current];
        } else {
            return 0;
        }
    }

    private char peekNext() {
        if (current + 1 < input.length) {
            return (char) input[current + 1];
        } else {
            return 0;
        }
    }

    private void skipBlockComments() {
        var endComment = false;
        while (!endComment) {
            advance();
            char ch = peek();

            if (ch == 0) { // eof
                throw new Error(String.format("expected %s, got %s instead", "*/", "EOF"));
            }

            if (Character.toString(ch).equals("\n")) {
                continue;
            }
            if (Character.toString(ch).equals("*")) {
                while (Character.toString(ch).equals("*")) {
                    advance();
                    ch = peek();
                }
                if (Character.toString(ch).equals("/")) {
                    endComment = true;
                    advance();
                }
            }
        }
    }

}
